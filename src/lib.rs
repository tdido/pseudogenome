use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::{Path, PathBuf};
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "tstk-rs",
    about = "Various tools for processing sequencing data"
)]
enum Opts {
    #[structopt(about = "Filter a multi-fasta file based on header contents")]
    Fastafilter {
        #[structopt(parse(from_os_str))]
        input_file: PathBuf,

        query: String,
    },
    #[structopt(about = "Merge a multi-fasta file into a single sequence")]
    Pseudogenome {
        #[structopt(parse(from_os_str))]
        input_file: PathBuf,

        #[structopt(short = "c", long, default_value = "N")]
        sep_char: String,

        #[structopt(short, long, default_value = "pseudogenome")]
        fasta_header: String,

        #[structopt(short, long, default_value = "50")]
        sep_width: usize,

        #[structopt(short, long, default_value = "80")]
        line_width: usize,
    },
}

fn read_lines<P>(path: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(&path)?;
    Ok(io::BufReader::new(file).lines())
}

pub fn run() -> Result<(), Box<dyn Error>> {
    match Opts::from_args() {
        Opts::Fastafilter { input_file, query } => {
            filter_fasta(input_file, &query)?;
        }
        Opts::Pseudogenome {
            input_file,
            sep_width: swidth,
            line_width: lwidth,
            sep_char: sep,
            fasta_header: header,
        } => {
            build_genome(input_file, &swidth, &lwidth, &sep, &header)?;
        }
    }

    Ok(())
}

fn filter_fasta(filename: PathBuf, query: &str) -> Result<(), Box<dyn Error>> {
    let mut print = false;
    if let Ok(lines) = read_lines(filename) {
        for line in lines {
            if let Ok(l) = line {
                if l.starts_with('>') {
                    if l.contains(query) {
                        print = true;
                    } else {
                        print = false;
                    }
                }
                if print {
                    println!("{}", l);
                }
            } else {
                return Err("Error reading lines".into());
            }
        }
    } else {
        return Err("No file here".into());
    }

    Ok(())
}

fn build_genome(
    filename: PathBuf,
    swidth: &usize,
    lwidth: &usize,
    sep: &str,
    header: &str,
) -> Result<(), Box<dyn Error>> {
    if let Ok(mut lines) = read_lines(filename) {
        println!(">{}", header);

        if !lines.next().unwrap().unwrap().starts_with('>') {
            return Err(
                "Input file doesn't start with a FASTA header. Check your formatting.".into(),
            );
        }

        let mut buffer = String::from("");

        for line in lines {
            if let Ok(l) = line {
                if l.starts_with('>') {
                    buffer.push_str(&sep.repeat(*swidth));
                    continue;
                }

                buffer.push_str(l.trim());

                if buffer.len() >= *lwidth {
                    println!("{}", &buffer[..*lwidth]);
                    buffer = String::from(&buffer[*lwidth..]);
                }
            } else {
                return Err("Error reading lines".into());
            }
        }
        println!("{}", &buffer);
    } else {
        return Err("No file here".into());
    }

    Ok(())
}
